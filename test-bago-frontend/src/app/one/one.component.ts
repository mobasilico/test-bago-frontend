import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styles: ['./one.component.scss']
})
export class OneComponent implements OnInit {

  api;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getDemoMethod().subscribe((data: any)=>{  
			this.api = data;  
		});
  }

}
